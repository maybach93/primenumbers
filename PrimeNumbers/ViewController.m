//
//  ViewController.m
//  PrimeNumbers
//
//  Created by Vitalii Poponov on 17.10.17.
//  Copyright © 2017 Vitalii Poponov. All rights reserved.
//

#import "ViewController.h"

NSInteger startNumber = 1000000000000000000.0;
NSInteger minimalPrimeNumber = 2;
NSTimeInterval defaultTimeInterval = 0.5;

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@end

@implementation ViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self runSearchPrimeNumbers];
}

#pragma mark - Private methods

- (void)runSearchPrimeNumbers {
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    dispatch_async(queue, ^{
        for (NSInteger i = startNumber; i >= minimalPrimeNumber; i--) {
            BOOL isPrimeNumberFound = YES;
            for (NSInteger j = minimalPrimeNumber; j <= sqrt(i); j++) {
                if (i % j == 0) {
                    isPrimeNumberFound = NO;
                    break;
                }
            }
            if (isPrimeNumberFound) {
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [self appendPrimeNumber: i];
                });
            }
        }
    });
}

- (void)appendPrimeNumber: (NSInteger)primeNumber {
    NSString *stringToAppend = [NSString stringWithFormat:@"%ld\n", primeNumber];
    self.textView.text = [self.textView.text stringByAppendingString:stringToAppend];
}

#pragma mark - Actions

- (IBAction)hideButton:(id)sender {
    [UIView animateWithDuration: defaultTimeInterval animations:^{
        [self.contentView setAlpha:0];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration: defaultTimeInterval animations:^{
            [self.contentView setAlpha:1];
        }];
    }];
}

@end
